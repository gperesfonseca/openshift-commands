# Atualização de cluster

1. Privilegio de Cluster-admin
1. Backup do etcd em caso de falha na atualização do cluster
1. Observar se todos os operadores instalados estão na ultima versão
1. Observar se todas as machine config pools (MCPs) estão com estado running e não paused
1. Se seu cluster usa credenciais mantidas manualmente, verifique se o Cloud Credential - Operator (CCO) está em um estado atualizável.
1. Realizar o reteiro da atualização no https://access.redhat.com/labs/ocpupgradegraph/update_path

## Itens necessarios checar antes da atualização

- pods
- Cluster Operators
- nodes
- mcps
- versão atual do cluster
- overview do cluster

## Backup do etcd

<!-- 1. Pequise os nós:
``oc get nodes`` -->

1. Pegue o ip do nó que será utilizado:<br> 
``oc get nodes -o wide``

1. Faça um debug no nó:<br>
``oc debug node/nome_do_nó``

1. Mude o seu diretorio root para o host:<br>
``sh-4.2# chroot /host``

1. Rode o cluster-backup.sh e passe o local para salvar o backup:<br>
``sh-4.4# /usr/local/bin/cluster-backup.sh /home/core/assets/backup``

1. Acesse o bastion

1. Realize o comando para trazer o arquivo do nó para o bastion:<br>
``scp usuario@ip:/caminho/arquivo .``

1. Caso não consiga trazer tente envia-lo a partir do nó para o bastion:<br>
``scp -r /home/core/assets/backup/(arquivo) usuario@ipdobastion:/(local)/(ondeVaiFicarO)/(Arquivo)/backup``

## Upgrade de operadores

### Mudando update chennel para um operador

1. Na perspectiva de administrador do OCP na console web, clique na barra lateral a esquerda em Operators -> Installed Operators.
1. Clique no nome do operador que deseja atualizar a versão.
1. Clique na aba Subscription.
1. Vá em update channel e clique na versão mais recente(stable) e clique em Save.

**Para subscriptions(subscrições) com a estrategia de aprovação automática o upgrade começa de forma automática.**

1. Navegue novamente para Operators -> installed Operators para monitorar o processo de upgrade.
1. Quando estiver concluido o status muda para Succeeded e Up to date.

## Aprovação manual de upgrade para operador pendente

**Se a estrategia de aprovação em subscription está como manual o update deve ser apovado manualmente.**

1. Na perspectiva de administrador do OCP na console web, navegue até Operators -> Installed Operators.
1. Esses operadores possuem um display com o status Upgrade available. clique no nome do operador que deseja fazer o upgrade.
1. Clique na aba Subscription. Todas as atualizações que requerem aprovação são exibidas ao lado de Upgrade Status.
1. Clique em Requires Approval e clique em Preview Install Plan.
1. Revise os recursos que estão listados com upgrade disponivel. Quando estiver satisfeito clique em Approve.
1. Navegue novamente para Operators -> installed Operators para monitorar o processo de upgrade.
1. Quando estiver concluido o status muda para Succeeded e Up to date.

## Observando se os mcp's estão running
1. Use o comando:<br>
``oc get mcp``

## Pausando o MachineHealthCheck versão 4.9+

**A partir da versão 4.9 a documentação traz a pausa para o MachineHealthCheck para evitar a reinicialização desses nós, pause todos os MachineHealthCheck antes de atualizar o cluster.**

1. Para listar todos os MachineHealthCheckrecursos disponíveis que você deseja pausar, execute o seguinte comando:<br>
``oc get machinehealthcheck -n openshift-machine-api``

1. Para pausar as verificações de integridade da máquina, adicione a **cluster.x-k8s.io/paused=""anotação** ao MachineHealthCheckrecurso. Execute o seguinte comando:<br>
``oc -n openshift-machine-api annotate mhc (nome-mhc) cluster.x-k8s.io/paused=""``

1. **Muito importante** que após a atualização seja retomada a verificação de integridade:<br>
``oc -n openshift-machine-api annotate mhc (nome-mhc) cluster.x-k8s.io/paused-``

---