oc get nodes

oc adm taint nodes <node-name> <taint-key>=<taint-value>:<taint-effect>

oc adm taint <node-name> env=prod:NoExecute

oc describe node <node-name>
