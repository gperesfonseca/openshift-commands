oc get nodes

oc label node <node-name> <label-key>=<label-value>

oc label node my-node environment=production

oc describe node <node-name>
