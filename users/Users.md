oc create user <username>

oc login -u system:admin

oc adm policy add-cluster-role-to-user cluster-admin <username>

oc adm policy add-role-to-user <role> <username>

oc get users

oc describe rolebindings -n <namespace> # (if applicable)

oc login -u <username> -p <password> <OpenShift_URL>
